import React from 'react';
import { Grid } from '@material-ui/core';
import VideoItem from './VideoItem';

const VideoList = ({videos, onVideoSelect}) => {
	const listOfVideos = videos.map((video, id) => <VideoItem key={id} video={video} onVideoSelect={onVideoSelect} />);

  return (
  	<div>
	  	{ videos.length > 0
	  		?
	  			(
	  				<Grid container spacing={10}>
	  					{ listOfVideos }
	  				</Grid>
	  			)
	  		: 
	  			(
	  				<p>No videos to show</p>
	  			)
	  	}
  	</div>
  );
}

export default VideoList;