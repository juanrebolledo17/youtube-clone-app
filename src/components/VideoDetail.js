import React from 'react';
import { Paper, Typography } from '@material-ui/core';

const VideoDetail = ({video, loading}) => {
	if (!loading && !video) {
		return (
			<p>Ups, couldn't find any videos!</p>
		);
	} else if (loading) {
		return (
			<React.Fragment>
				<div>Loading...</div>
				<div className="loading-indicator"></div>
			</React.Fragment>
		);
	}

	const videoSrc = `https://www.youtube.com/embed/${video.id.videoId}`;

  return (
  	<>
  		<Paper elevation={6} style={{ height: '70%' }}>
  			<iframe frameBorder="0" height="100%" width="100%" title="Video Player" src={videoSrc} />
  		</Paper>
  		<Paper elevation={6} style={{ padding: '15px' }}>
  			<Typography variant="h4"> {video.snippet.title} - {video.snippet.channelTitle}</Typography>
  			<Typography variant="subtitle1"> {video.snippet.channelTitle}</Typography>
  			<Typography variant="subtitle2"> {video.snippet.description}</Typography>
  		</Paper>
  	</>
  );
}

export default VideoDetail;

// { loading ? 
// 	(
// 		<>
// 			<div>Loading...</div>
// 			<div className="loading-indicator"></div>
// 		</>
// 	)
// :
// 	(
// 		<p>Cannot request videos, you are offline!</p>
// 	)
// }