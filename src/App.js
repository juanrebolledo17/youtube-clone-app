import React, { useEffect, useState } from 'react';
import youtube from './api/youtube';
import { Grid } from '@material-ui/core';
import { SearchBar, VideoList, VideoDetail } from './components';
import './App.css';

const App = () => {
	const [videos, setVideos] = useState([]);
	const [selectedVideo, setSelectedVideo] = useState(null);
	const [loading, setLoading] = useState(true);

	useEffect(searchTerm => {
		const response = youtube.get('search', {
			params: {
				part: 'snippet',
				maxResults: 5,
				key: '[API_KEY]',
				q: searchTerm,
			}
		});

		handleSubmit(response);
	});

	useEffect(() => {
		setTimeout(() => {
			setLoading(false);
		}, 3000);
	});

	const handleSubmit = videos => {
		if (videos.length) {
			setVideos(videos.data.items);
			setSelectedVideo(videos.data.items[0]);
		} else {
			// setLoading(true);
			// console.log('Cannot request videos, you are offline!');
		}
	}

	// function handleSubmit(videos) {
	// 	setVideos(videos.data.items);
	// 	setSelectedVideo(videos.data.items[0]);
	// }

	const onVideoSelect = video => {
		setSelectedVideo(video);
	}

	// function onVideoSelect(video) {
	// 	setSelectedVideo(video);
	// }

	return (
		<Grid style={{ justifyContent: 'center' }} container spacing={10}>
		  <Grid item xs={11}>
		    <Grid container spacing={10}>
		      <Grid item xs={12}>
		      	<SearchBar onFormSubmit={handleSubmit} />
		      </Grid>
		      <Grid item xs={8}>
		      	<VideoDetail video={selectedVideo} loading={loading} />
		      </Grid>
		      <Grid item xs={4}>
		      	<VideoList videos={videos} onVideoSelect={onVideoSelect} />
		      </Grid>
		    </Grid>
		  </Grid>
		</Grid>
	);
}

export default App;


// class App extends React.Component {
// 	state = {
// 		videos: [],
// 		selectedVideo: null
// 	};

// 	handleSubmit = async searchTerm => {
// 		const response = await youtube.get('search', {
// 			params: {
// 				part: 'snippet',
// 				maxResults: 5,
// 				key: '[API_KEY]',
// 				q: searchTerm,
// 			}
// 		});

// 		this.setState({ videos: response.data.items, selectedVideo: response.data.items[0] });
// 	}

// 	onVideoSelect = video => {
// 		this.setState({selectedVideo: video});
// 	}

// 	render() {
// 		const { selectedVideo, videos } = this.state;

//     return (
//       <Grid style={{ justifyContent: 'center' }} container spacing={10}>
//         <Grid item xs={11}>
//           <Grid container spacing={10}>
//             <Grid item xs={12}>
//             	<SearchBar onFormSubmit={this.handleSubmit} />
//             </Grid>
//             <Grid item xs={8}>
//             	<VideoDetail video={selectedVideo} />
//             </Grid>
//             <Grid item xs={4}>
//             	<VideoList videos={videos} onVideoSelect={this.onVideoSelect} />
//             </Grid>
//           </Grid>
//         </Grid>
//       </Grid>
//     );
//   }
// }